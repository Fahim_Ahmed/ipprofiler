import javax.xml.bind.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * User: Fahim
 * Date: 5/9/2015
 * Time: 10:39 PM
 */


@XmlRootElement(name = "Profile")
@XmlType(propOrder={"validator", "name", "networkName", "ip", "mask", "gateway", "dns1", "dns2", "dnsOnly"})
public class ProfileXml {

    String validator;

    String name;
    String networkName;
    String ip;
    String mask;
    String gateway;
    String dns1;
    String dns2;
    boolean dnsOnly;

    public ProfileXml(){
        this.validator = "bhungchung";
    }

    @XmlElement(name = "validator")
    public void setValidator(String validator) {
        this.validator = validator;
    }

    @XmlElement(name = "Name")
    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "Network")
    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    @XmlElement(name = "IP")
    public void setIp(String ip) {
        this.ip = ip;
    }

    @XmlElement(name = "Mask")
    public void setMask(String mask) {
        this.mask = mask;
    }

    @XmlElement(name = "Gateway")
    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    @XmlElement(name = "DNS1")
    public void setDns1(String dns1) {
        this.dns1 = dns1;
    }

    @XmlElement(name = "DNS2")
    public void setDns2(String dns2) {
        this.dns2 = dns2;
    }

    @XmlElement(name = "DNSOnly")
    public void setDnsOnly(boolean dnsOnly) { this.dnsOnly = dnsOnly; }

    public String getValidator() {
        return validator;
    }

    public String getName() {
        return name;
    }

    public String getNetworkName() {
        return networkName;
    }

    public String getIp() {
        return ip;
    }

    public String getMask() {
        return mask;
    }

    public String getGateway() {
        return gateway;
    }

    public String getDns1() {
        return dns1;
    }

    public String getDns2() {
        return dns2;
    }

    public boolean isDnsOnly() { return dnsOnly; }

    @Override
    public String toString() {
        return this.name;
    }
}
