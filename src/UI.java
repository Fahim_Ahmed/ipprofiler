import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


/**
 * @author fahim ahmed
 */
public class UI extends JFrame {

    private Pattern pattern;
    private Matcher matcher;

    private static final String IPADDRESS_PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    private String profileDir = "Profiles";
    private boolean isOk = true;
    private boolean isValid = true;

    private DefaultListModel<ProfileXml> listModel;

    public UI() {
        initComponents();
        setIcon();

        pattern = Pattern.compile(IPADDRESS_PATTERN);

        initProfileList();
        initNetworkList();
        readProfiles();
    }

    private void setProfileValues(ProfileXml px) {
        tfName.setText(px.getName());
        netbox.getEditor().setItem(px.getNetworkName());
        tfIp.setText(px.getIp());
        tfMask.setText(px.getMask());
        tfGateway.setText(px.getGateway());
        tfDns1.setText(px.getDns1());
        tfDns2.setText(px.getDns2());
        checkDns.setSelected(px.isDnsOnly());
    }

    public boolean validateIP(final String ip){
        matcher = pattern.matcher(ip);
        return matcher.matches();
    }

    void showWarning(String s){
        lStat.setForeground(new Color(255, 75, 111));
        lStat.setText(s);
    }

    void showStatus(String s){
        lStat.setForeground(new Color(0, 255, 153));
        lStat.setText(s);
    }

    private void readProfiles(){
        System.out.println("-----> scanning...");
        showStatus("Scanning for profiles...");

        isOk = false;

        new Thread(new Runnable() {
            @Override
            public void run() {

                File file = new File(profileDir);
                if(!file.exists()){
                    showStatus("...zZ");
                    isOk = true;
                    return;
                }

                File files[] = file.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        String str = f.getName();
                        return str.endsWith(".profile");
                    }
                });

                for(File f : files){
                    ProfileXml p = readXml(f.getName());
                    if(p != null){
                        if(hasAlready(p) == -1) listModel.addElement(p);
                    }
                }

                showStatus("...zZ");
                isOk = true;
            }
        }).start();
    }

    private int hasAlready(ProfileXml p) {
        int len = listModel.size();
        for (int i = 0; i < len; i++){
            ProfileXml px = listModel.getElementAt(i);
            if(px.getName().equalsIgnoreCase(p.getName())) return i;
        }
        return -1;
    }

    private ProfileXml readXml(String name){
        try {
            //System.out.println("-----> " + profileDir + "/" + name);
            File file = new File(profileDir + "/" + name);

            JAXBContext jaxbContext = JAXBContext.newInstance(ProfileXml.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            return (ProfileXml) jaxbUnmarshaller.unmarshal(file);

        } catch (JAXBException ex) {
            showWarning(ex.getErrorCode() + ": " + ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    private void btnCreateActionPerformed(ActionEvent e) {
        if(isOk) {
            isValid = true;
            validateFields();
            if (isValid) createXML();
        }else{
            showWarning("shhuush...");
        }
    }

    private void createXML() {
        showStatus("Writing profile...");

        ProfileXml px = new ProfileXml();

        px.setValidator("bhungchung");
        px.setName(tfName.getText());
        px.setNetworkName((String) netbox.getEditor().getItem());
        px.setIp(tfIp.getText());
        px.setMask(tfMask.getText());
        px.setGateway(tfGateway.getText());
        px.setDns1(tfDns1.getText());
        px.setDns2(tfDns2.getText());
        px.setDnsOnly(checkDns.isSelected());

        int index = hasAlready(px);
        if(index != -1) {
            showWarning("Duplicate profile name.");

            int n = JOptionPane.showConfirmDialog(this, "Update existing profile?", "Warning", JOptionPane.YES_NO_OPTION);

            if(n == JOptionPane.NO_OPTION){
                showStatus("...zZ");
                return;
            }
            if(n == JOptionPane.YES_OPTION){
                listModel.removeElementAt(index);
                listModel.add(index, px);
            }

            showStatus("Writing profile...");
        } else {
            listModel.addElement(px);
        }

        try{
            File dir = new File(profileDir);
            if(!dir.exists()) {
                if (!dir.mkdir()) {
                    showWarning("Permission error. Sigh.");
                    return;
                }
            }

            File file = new File(profileDir + "/" + tfName.getText() + ".profile");
            JAXBContext jaxbContext = JAXBContext.newInstance(ProfileXml.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(px, file);
            jaxbMarshaller.marshal(px, System.out);


        }catch (JAXBException ex){
            showWarning(ex.getErrorCode()+": "+ex.getMessage());
            ex.printStackTrace();
        }

        showStatus("Profile created.");
    }

    private void validateFields() {

        if((tfName.getText() == null) || (tfName.getText().trim().length() == 0)){
            showWarning("Invalid name. I know this is hard.");
            isValid = false;
            return;
        }

        String network = (String) netbox.getEditor().getItem();
        if((network == null) || (network.trim().length() == 0)){
            showWarning("Invalid network connection name.\nMore info at http://www.google.com");
            isValid = false;
            return;
        }

        if(!checkDns.isSelected()) {
            if (!validateIP(tfIp.getText())) {
                showWarning("Invalid IP.");
                isValid = false;
                return;
            }
            if (!validateIP(tfMask.getText())) {
                showWarning("Invalid subnet mask.");
                isValid = false;
                return;
            }
            if (!validateIP(tfGateway.getText())) {
                showWarning("Invalid gateway.");
                isValid = false;
                return;
            }
        }

        if(!((tfDns1.getText() == null) || (tfDns1.getText().trim().length() == 0))) {
            if (!validateIP(tfDns1.getText())) {
                showWarning("Invalid DNS 1.");
                isValid = false;
                return;
            }
        }

        if(!((tfDns2.getText() == null) || (tfDns2.getText().trim().length() == 0))) {
            if (!validateIP(tfDns2.getText())) {
                showWarning("Invalid DNS 2.");
                isValid = false;
            }
        }
    }

    private void tfFocusGained(FocusEvent e) {
        JTextField tf = (JTextField) e.getSource();
        tf.selectAll();
    }

    private void btnRemoveActionPerformed(ActionEvent e) {
        String name = tfName.getText();
        int index = list.getSelectedIndex();
        if(index != -1) listModel.removeElementAt(index);

        File f = new File(profileDir + "/" + name + ".profile");
        f.delete();

        showStatus("So it goes.");
    }

    private void btnRefreshActionPerformed(ActionEvent e) {
        readProfiles();
    }

    private void btnResetActionPerformed(ActionEvent e) {
        if(!isOk){
            showWarning("shhuush...");
            return;
        }

        showStatus("Resetting...");

        String[] command1 = {"netsh", "interface", "ip", "set", "address", "name="+ netbox.getEditor().getItem(), "source=dhcp" }; //for main addresses
        String[] command2 = {"netsh", "interface", "ip", "set", "dnsservers", "name="+ netbox.getEditor().getItem(), "source=dhcp" }; //for dnsservers
        try {
            Process process1 = Runtime.getRuntime().exec(command1);
            Process process2 = Runtime.getRuntime().exec(command2);

            BufferedReader reader = new BufferedReader(new InputStreamReader(process1.getInputStream()));
            String str;
            while((str = reader.readLine()) != null){
                System.out.println("-----> " + str);
                if(str.trim().length() != 0){
                    if(str.contains("DHCP is already enabled on this interface.")) {
                        fakeLoader();
                        showStatus("Most probably it's done.");
                        return;
                    }

                    showWarning(str + " Most probably wrong network name.");
                    //resetOnError();
                    return;
                }
            }

        } catch (IOException e1) {
            e1.printStackTrace();
            showWarning("Sigh.\n" + e1.getMessage());
            return;
        }

        fakeLoader();
        //showStatus("Most probably it's done.");
    }

    private void btnApplyActionPerformed(ActionEvent e) {
        if(!isOk){
            showWarning("shhuush...");
            return;
        }

        isValid = true;
        validateFields();
        if(!isValid) return;

        showStatus("Applying...");

        String[] command2 = {"netsh", "interface", "ip", "add", "dnsservers", "name="+netbox.getEditor().getItem(), tfDns1.getText(), "index=1", "validate=no" }; //for dnsservers 1
        String[] command3 = {"netsh", "interface", "ip", "add", "dnsservers", "name="+netbox.getEditor().getItem(), tfDns2.getText(), "index=2", "validate=no" }; //for dnsservers 2
        try {
            Process process1 = null;
            if(!checkDns.isSelected()){
                String[] command1 = {"netsh", "interface", "ip", "set", "address", "name="+netbox.getEditor().getItem(), "source=static", tfIp.getText(), tfMask.getText(), tfGateway.getText() }; //for main addresses
                process1 = Runtime.getRuntime().exec(command1);
            }

            Process process2 = Runtime.getRuntime().exec(command2);
            Process process3 = Runtime.getRuntime().exec(command3);

            if(!checkDns.isSelected()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(process1.getInputStream()));
                String str;
                while ((str = reader.readLine()) != null) {
                    System.out.println("-----> " + str);
                    if (str.trim().length() != 0) {
                        showWarning(str + " Typo?");
                        resetOnError();
                        return;
                    }
                }
            }

        } catch (IOException e1) {
            e1.printStackTrace();
            showWarning("Sigh.\n" + e1.getMessage());
            return;
        }

        fakeLoader();
    }

    private void initProfileList() {
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        listModel= new DefaultListModel<ProfileXml>();
        list.setModel(listModel);

        listModel.addListDataListener(new ListDataListener() {
            @Override
            public void intervalAdded(ListDataEvent e) {

            }

            @Override
            public void intervalRemoved(ListDataEvent e) {

            }

            @Override
            public void contentsChanged(ListDataEvent e) {
                list.updateUI();
            }
        });

        list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) {
                    ProfileXml px = (ProfileXml) list.getSelectedValue();
                    if(px != null) setProfileValues(px);
                }
            }
        });
    }

    private void initNetworkList() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ComboBoxModel<String> boxModel = null;
                String[] command = {"netsh", "interface", "show", "interface"};
                try {
                    Process process = Runtime.getRuntime().exec(command);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

                    Vector<String> interfaces = new Vector<String>();
                    String str;
                    while ((str = reader.readLine()) != null){

                        //System.out.println("-----> " +str);

                        if(str.contains("State") || str.startsWith("---") || str.trim().length() == 0) continue;
                        interfaces.add(str.substring(47));

                        System.out.println("-----> " +interfaces.lastElement());
                    }

                    String[] conns = new String[interfaces.size()];

                    for (int i = 0; i < conns.length; i++) {
                        conns[i] = interfaces.get(i);
                    }

                    boxModel = new DefaultComboBoxModel<String>(conns);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(boxModel != null){
                    netbox.setModel(boxModel);
                    AutoCompleteDecorator.decorate(netbox);
                }
            }
        }).start();

        netbox.getEditor().setItem("Local Area Connection");
    }

    private void fakeLoader(){
        isOk = false;

        new Thread(new Runnable() {
            @Override
            public void run() {
                String str = "";
                int t = 30;
                for(int i = 0; i < 40; i++){
                    str += ". ";
                    showStatus(str);

                    try {
                        Thread.sleep(t);
                        t *= 0.95;
                        if(t < 10) t = 10;
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }

                isOk = true;
                showStatus("Most probably it's done.");
            }
        }).start();
    }

    private void resetOnError(){
        String[] c1 = {"netsh", "interface", "ip", "set", "address", "name="+netbox.getEditor().getItem(), "source=dhcp" }; //for main addresses
        String[] c2 = {"netsh", "interface", "ip", "set", "dnsservers", "name="+netbox.getEditor().getItem(), "source=dhcp" }; //for dnsservers
        try {
            Process p1 = Runtime.getRuntime().exec(c1);
            Process p2 = Runtime.getRuntime().exec(c2);

            BufferedReader reader = new BufferedReader(new InputStreamReader(p1.getInputStream()));
            String str;
            while((str = reader.readLine()) != null){
                System.out.println("-----> " + str);
                if(str.trim().length() != 0){
                    if(str.contains("DHCP is already enabled on this interface."))
                        return;

                    showWarning(str + " Most probably wrong network name.");
                    //resetOnError();
                    return;
                }
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            showWarning("Sigh.\n" + e1.getMessage());
        }
    }

    private void setIcon(){
        try {
            ArrayList<Image> icons = new ArrayList<Image>();
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-128.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-128.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-96.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-64.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-48.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-32.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-24.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-16.png")));

            setIconImages(icons);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }

        //super.setIconImage(image);
    }
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        panel1 = new JPanel();
        label1 = new JLabel();
        tfIp = new JTextField();
        label2 = new JLabel();
        tfMask = new JTextField();
        tfGateway = new JTextField();
        label3 = new JLabel();
        tfDns1 = new JTextField();
        label4 = new JLabel();
        label5 = new JLabel();
        tfDns2 = new JTextField();
        btnCreate = new JButton();
        tfName = new JTextField();
        label6 = new JLabel();
        btnApply = new JButton();
        btnReset = new JButton();
        lStat = new JTextArea();
        checkDns = new JCheckBox();
        netbox = new JComboBox();
        label7 = new JLabel();
        scrollPane1 = new JScrollPane();
        list = new JList();
        label8 = new JLabel();
        btnRefresh = new JButton();
        btnRemove = new JButton();

        //======== this ========
        setTitle("IPProfiler");
        setBackground(new Color(42, 44, 45));
        setResizable(false);
        Container contentPane = getContentPane();

        //======== panel1 ========
        {
            panel1.setBackground(new Color(42, 44, 45));

            //---- label1 ----
            label1.setText("IP");
            label1.setToolTipText("e.g. 192.168.1.100");

            //---- tfIp ----
            tfIp.setToolTipText("e.g. 192.168.1.100");
            tfIp.setText("192.168.1.100");
            tfIp.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    tfFocusGained(e);
                }
            });

            //---- label2 ----
            label2.setText("Mask");
            label2.setToolTipText("e.g. 255.255.255.0");

            //---- tfMask ----
            tfMask.setToolTipText("e.g. 255.255.255.0");
            tfMask.setText("255.255.255.0");
            tfMask.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    tfFocusGained(e);
                }
            });

            //---- tfGateway ----
            tfGateway.setToolTipText("e.g. 192.168.1.1");
            tfGateway.setText("192.168.1.1");
            tfGateway.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    tfFocusGained(e);
                }
            });

            //---- label3 ----
            label3.setText("Gateway");
            label3.setToolTipText("e.g. 192.168.1.1");

            //---- tfDns1 ----
            tfDns1.setText("8.8.8.8");
            tfDns1.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    tfFocusGained(e);
                }
            });

            //---- label4 ----
            label4.setText("DNS 1");

            //---- label5 ----
            label5.setText("DNS 2");

            //---- tfDns2 ----
            tfDns2.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    tfFocusGained(e);
                }
            });

            //---- btnCreate ----
            btnCreate.setText("Create Profile");
            btnCreate.setToolTipText("Set to DHCP mode.");
            btnCreate.setBackground(new Color(42, 44, 45));
            btnCreate.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnCreateActionPerformed(e);
                }
            });

            //---- tfName ----
            tfName.setToolTipText("e.g. Mars");
            tfName.setText("zZ");
            tfName.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                    tfFocusGained(e);
                }
            });

            //---- label6 ----
            label6.setText("Name");
            label6.setToolTipText("e.g. Bhau");

            //---- btnApply ----
            btnApply.setText("Apply");
            btnApply.setBackground(new Color(42, 44, 45));
            btnApply.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnApplyActionPerformed(e);
                }
            });

            //---- btnReset ----
            btnReset.setText("Reset");
            btnReset.setToolTipText("Set to DHCP mode.");
            btnReset.setBackground(new Color(42, 44, 45));
            btnReset.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    btnResetActionPerformed(e);
                }
            });

            //---- lStat ----
            lStat.setText("..zZ");
            lStat.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
            lStat.setForeground(new Color(0, 255, 153));
            lStat.setBackground(new Color(42, 44, 45));
            lStat.setFocusable(false);
            lStat.setRequestFocusEnabled(false);
            lStat.setLineWrap(true);
            lStat.setEditable(false);
            lStat.setCaretPosition(4);
            lStat.setWrapStyleWord(true);

            //---- checkDns ----
            checkDns.setText("DNS Only");
            checkDns.setBackground(new Color(42, 44, 45));

            //---- netbox ----
            netbox.setEditable(true);
            netbox.setBackground(new Color(42, 44, 45));

            //---- label7 ----
            label7.setText("Network");
            label7.setToolTipText("e.g. Local Area Connection");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(lStat, GroupLayout.PREFERRED_SIZE, 306, GroupLayout.PREFERRED_SIZE))
                            .addGroup(GroupLayout.Alignment.LEADING, panel1Layout.createSequentialGroup()
                                .addGap(48, 48, 48)
                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addComponent(btnReset, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 312, GroupLayout.PREFERRED_SIZE)
                                        .addGroup(GroupLayout.Alignment.LEADING, panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                            .addComponent(btnCreate, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 312, GroupLayout.PREFERRED_SIZE)
                                            .addGroup(panel1Layout.createParallelGroup()
                                                .addGroup(panel1Layout.createSequentialGroup()
                                                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                                                    .addGap(6, 6, 6)
                                                    .addComponent(tfDns1, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(label3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(label2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(tfGateway)
                                                        .addComponent(tfMask, GroupLayout.Alignment.LEADING)
                                                        .addComponent(tfIp, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE))))
                                            .addGroup(panel1Layout.createSequentialGroup()
                                                .addComponent(label5, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                                                .addGap(6, 6, 6)
                                                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(checkDns, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(tfDns2))))
                                        .addComponent(btnApply, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 312, GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panel1Layout.createSequentialGroup()
                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                            .addComponent(label6, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label7, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                            .addComponent(netbox, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(tfName))))))
                        .addContainerGap(54, Short.MAX_VALUE))
            );
            panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(label6))
                            .addComponent(tfName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                            .addComponent(label7)
                            .addComponent(netbox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(tfIp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addComponent(label1))
                        .addGroup(panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfMask, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(label2)))
                        .addGroup(panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfGateway, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(label3)))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(label4))
                            .addComponent(tfDns1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup()
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(label5))
                            .addComponent(tfDns2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(checkDns)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCreate)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnReset)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnApply, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lStat, GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                        .addContainerGap())
            );
        }

        //======== scrollPane1 ========
        {
            scrollPane1.setBackground(new Color(42, 44, 45));
            scrollPane1.setBorder(null);

            //---- list ----
            list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            list.setVisibleRowCount(80);
            list.setSelectionBackground(new Color(60, 63, 65, 0));
            list.setSelectionForeground(new Color(246, 174, 51));
            list.setBorder(null);
            scrollPane1.setViewportView(list);
        }

        //---- label8 ----
        label8.setText("PROFILES");
        label8.setHorizontalAlignment(SwingConstants.CENTER);
        label8.setForeground(new Color(246, 174, 51));
        label8.setBorder(null);

        //---- btnRefresh ----
        btnRefresh.setText("Refresh");
        btnRefresh.setBorder(null);
        btnRefresh.setBorderPainted(false);
        btnRefresh.setBackground(new Color(42, 44, 45));
        btnRefresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRefreshActionPerformed(e);
            }
        });

        //---- btnRemove ----
        btnRemove.setText("Remove");
        btnRemove.setBorder(null);
        btnRemove.setBackground(new Color(42, 44, 45));
        btnRemove.setBorderPainted(false);
        btnRemove.setForeground(new Color(246, 174, 51));
        btnRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRemoveActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(btnRemove, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(label8, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 2, Short.MAX_VALUE))
                        .addComponent(btnRefresh, GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                        .addComponent(scrollPane1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addComponent(label8, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemove, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRefresh, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        setSize(560, 500);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel panel1;
    private JLabel label1;
    private JTextField tfIp;
    private JLabel label2;
    private JTextField tfMask;
    private JTextField tfGateway;
    private JLabel label3;
    private JTextField tfDns1;
    private JLabel label4;
    private JLabel label5;
    private JTextField tfDns2;
    private JButton btnCreate;
    private JTextField tfName;
    private JLabel label6;
    private JButton btnApply;
    private JButton btnReset;
    private JTextArea lStat;
    private JCheckBox checkDns;
    private JComboBox netbox;
    private JLabel label7;
    private JScrollPane scrollPane1;
    private JList list;
    private JLabel label8;
    private JButton btnRefresh;
    private JButton btnRemove;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
